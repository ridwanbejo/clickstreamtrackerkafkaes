from kafka import KafkaProducer
from datetime import datetime

producer = KafkaProducer(bootstrap_servers=['127.0.0.1:9092'])

for i in range(10):
    producer.send('try-kafka', b'Hello world! - ' + str(datetime.now()) ) 

producer.flush()
producer.close()