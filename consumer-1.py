from kafka import KafkaConsumer

consumer = KafkaConsumer('try-kafka', 
								 auto_offset_reset='latest',
                                 # enable_auto_commit=True,
                                 # auto_commit_interval_ms=500,
                                 # consumer_timeout_ms=3000,
                                 bootstrap_servers=['127.0.0.1:9092'])

for message in consumer:
    print(message)

# consumer.close()