from kafka import KafkaConsumer, TopicPartition
import json

consumer = KafkaConsumer("try-kafka-three", 
						 group_id="try-kafka-three-group",
						 auto_offset_reset='latest',
                         value_deserializer=lambda m: json.loads(m.decode('ascii')),
                         bootstrap_servers=['127.0.0.1:9092'])

for message in consumer:
    print(message)
   
consumer.close()