from kafka import KafkaConsumer, TopicPartition
import json

consumer = KafkaConsumer(auto_offset_reset='latest',
                         value_deserializer=lambda m: json.loads(m.decode('ascii')),
                         bootstrap_servers=['127.0.0.1:9092'])
consumer.assign([TopicPartition('try-kafka-three', 3)])

for message in consumer:
    print(message)
    
consumer.close()