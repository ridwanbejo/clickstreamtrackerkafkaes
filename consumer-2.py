from kafka import KafkaConsumer
import json

consumer = KafkaConsumer('try-kafka-two', 
								 auto_offset_reset='latest',
                                 value_deserializer=lambda m: json.loads(m.decode('ascii')),
                                 bootstrap_servers=['127.0.0.1:9092'])

for message in consumer:
    # print(message)
    print (str(message.timestamp) + ": " + str(message.value))

consumer.close()