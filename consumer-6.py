from kafka import KafkaConsumer, TopicPartition
import elasticsearch
import json

from datetime import datetime
from elasticsearch import Elasticsearch
from uuid import uuid4

"""
create index on elasticsearch for this structure:

{ 
	'userId':'abcdef12345', 
	'mouse_position_x':randint(0, 1500), 
	'mouse_position_y':randint(0, 1500),
	'sourceUrl':'http://localhost:8080/login', 
	'createdAt':str(datetime.now()) 
}

curl -XPUT http://localhost:9200/mylog/

{ "mappings": {
  "clickstreamLog":{
    "properties":{
      "mouse_position_x":{
        "type":"integer"
      },
      "mouse_position_y":{
        "type":"integer"
      },
      "sourceUrl": {
        "type":"text"
      },
      "userId": {
        "type":"text"
      },
      "createdAt":{
        "type" :"date"
      }
    }
  }
} }


"""
es = Elasticsearch()

consumer = KafkaConsumer("try-kafka-three", 
						 group_id="try-kafka-three-group",
						 auto_offset_reset='earliest',
                         enable_auto_commit=True,
						 value_deserializer=lambda m: json.loads(m.decode('ascii')),
                         bootstrap_servers=['127.0.0.1:9092'])

for message in consumer:

	doc = { 
		'userId':message.value['userId'], 
		'mouse_position_x':message.value['mouse_position_x'], 
		'mouse_position_y':message.value['mouse_position_y'],
		'sourceUrl':message.value['sourceUrl'], 
		'createdAt':"2017-01-01"
	}

	res = es.index(index="mylog", doc_type='clickstreamLog', id=str(uuid4()), body=doc)

	print(res)
	print(message)
   
consumer.close()