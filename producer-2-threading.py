from kafka import KafkaProducer
from datetime import datetime
import json
import time
import threading

def send_message():
	producer = KafkaProducer(	
						value_serializer=lambda m: json.dumps(m).encode('ascii'),
						bootstrap_servers=['127.0.0.1:9092']
					)

	time.sleep(0.2)
	print ("Message!")
	
	producer.send('try-kafka-two', { 'senderId':'abcdef12345', 'msg':'Hello world!', 'createdAt':str(datetime.now()) }) 
	
	producer.flush()
	producer.close()

# while True:
threads = []

for i in range(5):
    t = threading.Thread( target=send_message )
    threads.append(t)
    t.start()
