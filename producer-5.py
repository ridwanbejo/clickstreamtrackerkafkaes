from kafka import KafkaProducer
from datetime import datetime
import json
import time
import threading
from random import randint

producer = KafkaProducer(	
						value_serializer=lambda m: json.dumps(m).encode('ascii'),
						bootstrap_servers=['127.0.0.1:9092']
					)

while True:
	time.sleep(0.2)
	partitionKey = "key-"+str(randint(0, 3))
	print (partitionKey)
	producer.send('try-kafka-three', key=partitionKey, value={ 'senderId':'abcdef12345', 'msg':'Hello world!', 'createdAt':str(datetime.now()) }) 

producer.flush()
producer.close()